FROM alpine
RUN apk add --no-cache python3
RUN pip3 install discord.py
COPY . /src
ENTRYPOINT ["python3", "/src/bot.py"]
