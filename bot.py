import discord
import random
import string
chops = [":rock:", ":paper:", ":scissors:"]

client = discord.Client()

@client.event
async def on_ready():
    print("logged in as")
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    if message.content.startswith('!rps'):
        response = str.format("{} threw {}",message.author.mention, random.choice(chops))
        # logging to STD out to begin.
        print(response, "in channel", message.channel)
        await client.send_message(message.channel,response)

if __name__ == "__main__":
    TOKEN = None
    with open('/run/secrets/rpsbot') as file:
        TOKEN = file.readline().strip()

    client.run(TOKEN)
